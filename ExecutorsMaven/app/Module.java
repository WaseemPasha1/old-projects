import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import controllers.HomeController;
import dao.*;
import org.hibernate.jpa.HibernatePersistenceProvider;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.constraints.Max;
import java.time.Clock;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by user on 18/8/17.
 */
public class Module  extends AbstractModule {
//    HibernatePersistenceProvider hibernatePersistenceProvider;
    @Override
    public void configure(){

        bind(Clock.class).toInstance(Clock.systemDefaultZone());
        bind(Customerdao.class).to(CustomerdaoImpl.class);
        bind(campaigndao.class).to(campaignDaoImpl.class);
        bind(SubscriptionUsageDao.class).to(SubscriptionUsageDaoImpl.class);
        bind(Notificationdao.class).to(NotificationdaoImpl.class);
    }

//    private static final ThreadLocal<EntityManager> ENTITY_MANAGER_CACHE = new ThreadLocal<EntityManager>();
//
//    @Provides
//    @Singleton
//    public EntityManagerFactory provideEntityManagerFactory() {
////        List providers = getProvider(org.hibernate.jpa.HibernatePersistenceProvider);
//        Map<String, String> properties = new HashMap<String, String>();
//        properties.put("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");
//        properties.put("hibernate.connection.url", "jdbc:mysql://localhost/createSch?autoReconnect=true&useSSL=false");
//        properties.put("hibernate.connection.username", "root");
//        properties.put("hibernate.connection.password", "password");
////        properties.put("hibernate.connection.pool_size", "1");
//        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
//        properties.put("hibernate.hibernate.show_sql.auto", "create");
//        properties.put("hibernate.cache.use.query_cache", "false");
//        properties.put("hibernate.cache.use_second_level_cache", "false");
//        properties.put("hibernate.hbm2ddl.import_files", "/import.sql");
//
//        return Persistence.createEntityManagerFactory("defaultPersistenceUnit", properties);
////        return null;
//    }
//    @Provides
//    public EntityManager provideEntityManager (EntityManagerFactory entityManagerFactory){
//        EntityManager entityManager = ENTITY_MANAGER_CACHE.get();
//        if (entityManager == null) {
//            ENTITY_MANAGER_CACHE.set(entityManager = entityManagerFactory.createEntityManager());
//        }
//        return entityManager;
//    }
}
