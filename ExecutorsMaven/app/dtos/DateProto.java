// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: protos/date.proto

package dtos;

public final class DateProto {
  private DateProto() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface DateOrBuilder extends
      // @@protoc_insertion_point(interface_extends:Date)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <pre>
     * Year of date. Must be from 1 to 9999, or 0 if specifying a date without
     * a year.
     * </pre>
     *
     * <code>int32 year = 1;</code>
     */
    int getYear();

    /**
     * <pre>
     * Month of year. Must be from 1 to 12.
     * </pre>
     *
     * <code>int32 month = 2;</code>
     */
    int getMonth();

    /**
     * <pre>
     * Day of month. Must be from 1 to 31 and valid for the year and month, or 0
     * if specifying a year/month where the day is not significant.
     * </pre>
     *
     * <code>int32 day = 3;</code>
     */
    int getDay();
  }
  /**
   * Protobuf type {@code Date}
   */
  public  static final class Date extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:Date)
      DateOrBuilder {
    // Use Date.newBuilder() to construct.
    private Date(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private Date() {
      year_ = 0;
      month_ = 0;
      day_ = 0;
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private Date(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 8: {

              year_ = input.readInt32();
              break;
            }
            case 16: {

              month_ = input.readInt32();
              break;
            }
            case 24: {

              day_ = input.readInt32();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return dtos.DateProto.internal_static_Date_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return dtos.DateProto.internal_static_Date_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              dtos.DateProto.Date.class, dtos.DateProto.Date.Builder.class);
    }

    public static final int YEAR_FIELD_NUMBER = 1;
    private int year_;
    /**
     * <pre>
     * Year of date. Must be from 1 to 9999, or 0 if specifying a date without
     * a year.
     * </pre>
     *
     * <code>int32 year = 1;</code>
     */
    public int getYear() {
      return year_;
    }

    public static final int MONTH_FIELD_NUMBER = 2;
    private int month_;
    /**
     * <pre>
     * Month of year. Must be from 1 to 12.
     * </pre>
     *
     * <code>int32 month = 2;</code>
     */
    public int getMonth() {
      return month_;
    }

    public static final int DAY_FIELD_NUMBER = 3;
    private int day_;
    /**
     * <pre>
     * Day of month. Must be from 1 to 31 and valid for the year and month, or 0
     * if specifying a year/month where the day is not significant.
     * </pre>
     *
     * <code>int32 day = 3;</code>
     */
    public int getDay() {
      return day_;
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (year_ != 0) {
        output.writeInt32(1, year_);
      }
      if (month_ != 0) {
        output.writeInt32(2, month_);
      }
      if (day_ != 0) {
        output.writeInt32(3, day_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (year_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(1, year_);
      }
      if (month_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(2, month_);
      }
      if (day_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(3, day_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof dtos.DateProto.Date)) {
        return super.equals(obj);
      }
      dtos.DateProto.Date other = (dtos.DateProto.Date) obj;

      boolean result = true;
      result = result && (getYear()
          == other.getYear());
      result = result && (getMonth()
          == other.getMonth());
      result = result && (getDay()
          == other.getDay());
      return result;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + YEAR_FIELD_NUMBER;
      hash = (53 * hash) + getYear();
      hash = (37 * hash) + MONTH_FIELD_NUMBER;
      hash = (53 * hash) + getMonth();
      hash = (37 * hash) + DAY_FIELD_NUMBER;
      hash = (53 * hash) + getDay();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static dtos.DateProto.Date parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static dtos.DateProto.Date parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static dtos.DateProto.Date parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static dtos.DateProto.Date parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static dtos.DateProto.Date parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static dtos.DateProto.Date parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static dtos.DateProto.Date parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static dtos.DateProto.Date parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static dtos.DateProto.Date parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static dtos.DateProto.Date parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(dtos.DateProto.Date prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code Date}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:Date)
        dtos.DateProto.DateOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return dtos.DateProto.internal_static_Date_descriptor;
      }

      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return dtos.DateProto.internal_static_Date_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                dtos.DateProto.Date.class, dtos.DateProto.Date.Builder.class);
      }

      // Construct using dtos.DateProto.Date.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        year_ = 0;

        month_ = 0;

        day_ = 0;

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return dtos.DateProto.internal_static_Date_descriptor;
      }

      public dtos.DateProto.Date getDefaultInstanceForType() {
        return dtos.DateProto.Date.getDefaultInstance();
      }

      public dtos.DateProto.Date build() {
        dtos.DateProto.Date result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public dtos.DateProto.Date buildPartial() {
        dtos.DateProto.Date result = new dtos.DateProto.Date(this);
        result.year_ = year_;
        result.month_ = month_;
        result.day_ = day_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.setField(field, value);
      }
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof dtos.DateProto.Date) {
          return mergeFrom((dtos.DateProto.Date)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(dtos.DateProto.Date other) {
        if (other == dtos.DateProto.Date.getDefaultInstance()) return this;
        if (other.getYear() != 0) {
          setYear(other.getYear());
        }
        if (other.getMonth() != 0) {
          setMonth(other.getMonth());
        }
        if (other.getDay() != 0) {
          setDay(other.getDay());
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        dtos.DateProto.Date parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (dtos.DateProto.Date) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int year_ ;
      /**
       * <pre>
       * Year of date. Must be from 1 to 9999, or 0 if specifying a date without
       * a year.
       * </pre>
       *
       * <code>int32 year = 1;</code>
       */
      public int getYear() {
        return year_;
      }
      /**
       * <pre>
       * Year of date. Must be from 1 to 9999, or 0 if specifying a date without
       * a year.
       * </pre>
       *
       * <code>int32 year = 1;</code>
       */
      public Builder setYear(int value) {
        
        year_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       * Year of date. Must be from 1 to 9999, or 0 if specifying a date without
       * a year.
       * </pre>
       *
       * <code>int32 year = 1;</code>
       */
      public Builder clearYear() {
        
        year_ = 0;
        onChanged();
        return this;
      }

      private int month_ ;
      /**
       * <pre>
       * Month of year. Must be from 1 to 12.
       * </pre>
       *
       * <code>int32 month = 2;</code>
       */
      public int getMonth() {
        return month_;
      }
      /**
       * <pre>
       * Month of year. Must be from 1 to 12.
       * </pre>
       *
       * <code>int32 month = 2;</code>
       */
      public Builder setMonth(int value) {
        
        month_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       * Month of year. Must be from 1 to 12.
       * </pre>
       *
       * <code>int32 month = 2;</code>
       */
      public Builder clearMonth() {
        
        month_ = 0;
        onChanged();
        return this;
      }

      private int day_ ;
      /**
       * <pre>
       * Day of month. Must be from 1 to 31 and valid for the year and month, or 0
       * if specifying a year/month where the day is not significant.
       * </pre>
       *
       * <code>int32 day = 3;</code>
       */
      public int getDay() {
        return day_;
      }
      /**
       * <pre>
       * Day of month. Must be from 1 to 31 and valid for the year and month, or 0
       * if specifying a year/month where the day is not significant.
       * </pre>
       *
       * <code>int32 day = 3;</code>
       */
      public Builder setDay(int value) {
        
        day_ = value;
        onChanged();
        return this;
      }
      /**
       * <pre>
       * Day of month. Must be from 1 to 31 and valid for the year and month, or 0
       * if specifying a year/month where the day is not significant.
       * </pre>
       *
       * <code>int32 day = 3;</code>
       */
      public Builder clearDay() {
        
        day_ = 0;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:Date)
    }

    // @@protoc_insertion_point(class_scope:Date)
    private static final dtos.DateProto.Date DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new dtos.DateProto.Date();
    }

    public static dtos.DateProto.Date getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<Date>
        PARSER = new com.google.protobuf.AbstractParser<Date>() {
      public Date parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new Date(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<Date> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<Date> getParserForType() {
      return PARSER;
    }

    public dtos.DateProto.Date getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_Date_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_Date_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\021protos/date.proto\"0\n\004Date\022\014\n\004year\030\001 \001(" +
      "\005\022\r\n\005month\030\002 \001(\005\022\013\n\003day\030\003 \001(\005B\021\n\004dtosB\tD" +
      "ateProtob\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_Date_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_Date_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_Date_descriptor,
        new java.lang.String[] { "Year", "Month", "Day", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
