// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: protos/enums/invoiced_for.proto

package dtos.enums;

public final class InvoicedForEnumProtos {
  private InvoicedForEnumProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  /**
   * <pre>
   * Invoices are generated for services opted by end user.
   * A desciptor indicating for what service is the invoice generated
   * NEXT_TAG 4
   * </pre>
   *
   * Protobuf enum {@code InvoicedFor}
   */
  public enum InvoicedFor
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>UNKNOWN = 0;</code>
     */
    UNKNOWN(0),
    /**
     * <code>USER_SUBSCRIPTION = 1;</code>
     */
    USER_SUBSCRIPTION(1),
    /**
     * <code>USER_SUBSCRIPTION_TOPUP = 2;</code>
     */
    USER_SUBSCRIPTION_TOPUP(2),
    /**
     * <code>OTHER = 3;</code>
     */
    OTHER(3),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>UNKNOWN = 0;</code>
     */
    public static final int UNKNOWN_VALUE = 0;
    /**
     * <code>USER_SUBSCRIPTION = 1;</code>
     */
    public static final int USER_SUBSCRIPTION_VALUE = 1;
    /**
     * <code>USER_SUBSCRIPTION_TOPUP = 2;</code>
     */
    public static final int USER_SUBSCRIPTION_TOPUP_VALUE = 2;
    /**
     * <code>OTHER = 3;</code>
     */
    public static final int OTHER_VALUE = 3;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static InvoicedFor valueOf(int value) {
      return forNumber(value);
    }

    public static InvoicedFor forNumber(int value) {
      switch (value) {
        case 0: return UNKNOWN;
        case 1: return USER_SUBSCRIPTION;
        case 2: return USER_SUBSCRIPTION_TOPUP;
        case 3: return OTHER;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<InvoicedFor>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        InvoicedFor> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<InvoicedFor>() {
            public InvoicedFor findValueByNumber(int number) {
              return InvoicedFor.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return dtos.enums.InvoicedForEnumProtos.getDescriptor().getEnumTypes().get(0);
    }

    private static final InvoicedFor[] VALUES = values();

    public static InvoicedFor valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private InvoicedFor(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:InvoicedFor)
  }


  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\037protos/enums/invoiced_for.proto*Y\n\013Inv" +
      "oicedFor\022\013\n\007UNKNOWN\020\000\022\025\n\021USER_SUBSCRIPTI" +
      "ON\020\001\022\033\n\027USER_SUBSCRIPTION_TOPUP\020\002\022\t\n\005OTH" +
      "ER\020\003B#\n\ndtos.enumsB\025InvoicedForEnumProto" +
      "sb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
