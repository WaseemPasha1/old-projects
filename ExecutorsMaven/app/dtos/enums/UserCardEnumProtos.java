// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: protos/enums/user_card.proto

package dtos.enums;

public final class UserCardEnumProtos {
  private UserCardEnumProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  /**
   * <pre>
   * NEXT_TAG 10
   * </pre>
   *
   * Protobuf enum {@code UserCard}
   */
  public enum UserCard
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>UNKNOWN_USER_CARD = 0;</code>
     */
    UNKNOWN_USER_CARD(0),
    /**
     * <code>USER_CARD_QUERY_BUILDER = 1;</code>
     */
    USER_CARD_QUERY_BUILDER(1),
    /**
     * <code>USER_CARD_QUERY_MANAGEMENT = 2;</code>
     */
    USER_CARD_QUERY_MANAGEMENT(2),
    /**
     * <code>USER_CARD_SIGNALS = 3;</code>
     */
    USER_CARD_SIGNALS(3),
    /**
     * <code>USER_CARD_PERFORMANCE_REPORT = 4;</code>
     */
    USER_CARD_PERFORMANCE_REPORT(4),
    /**
     * <code>USER_CARD_CUSTOM_RESEARCH_REQUESTS = 5;</code>
     */
    USER_CARD_CUSTOM_RESEARCH_REQUESTS(5),
    /**
     * <code>USER_CARD_USER_HISTORY = 6;</code>
     */
    USER_CARD_USER_HISTORY(6),
    /**
     * <code>USER_CARD_INTEGRATION_MANAGEMENT = 7;</code>
     */
    USER_CARD_INTEGRATION_MANAGEMENT(7),
    /**
     * <code>USER_CARD_USER_ADMINISTRATION = 8;</code>
     */
    USER_CARD_USER_ADMINISTRATION(8),
    /**
     * <code>USER_CARD_BILLING = 9;</code>
     */
    USER_CARD_BILLING(9),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>UNKNOWN_USER_CARD = 0;</code>
     */
    public static final int UNKNOWN_USER_CARD_VALUE = 0;
    /**
     * <code>USER_CARD_QUERY_BUILDER = 1;</code>
     */
    public static final int USER_CARD_QUERY_BUILDER_VALUE = 1;
    /**
     * <code>USER_CARD_QUERY_MANAGEMENT = 2;</code>
     */
    public static final int USER_CARD_QUERY_MANAGEMENT_VALUE = 2;
    /**
     * <code>USER_CARD_SIGNALS = 3;</code>
     */
    public static final int USER_CARD_SIGNALS_VALUE = 3;
    /**
     * <code>USER_CARD_PERFORMANCE_REPORT = 4;</code>
     */
    public static final int USER_CARD_PERFORMANCE_REPORT_VALUE = 4;
    /**
     * <code>USER_CARD_CUSTOM_RESEARCH_REQUESTS = 5;</code>
     */
    public static final int USER_CARD_CUSTOM_RESEARCH_REQUESTS_VALUE = 5;
    /**
     * <code>USER_CARD_USER_HISTORY = 6;</code>
     */
    public static final int USER_CARD_USER_HISTORY_VALUE = 6;
    /**
     * <code>USER_CARD_INTEGRATION_MANAGEMENT = 7;</code>
     */
    public static final int USER_CARD_INTEGRATION_MANAGEMENT_VALUE = 7;
    /**
     * <code>USER_CARD_USER_ADMINISTRATION = 8;</code>
     */
    public static final int USER_CARD_USER_ADMINISTRATION_VALUE = 8;
    /**
     * <code>USER_CARD_BILLING = 9;</code>
     */
    public static final int USER_CARD_BILLING_VALUE = 9;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static UserCard valueOf(int value) {
      return forNumber(value);
    }

    public static UserCard forNumber(int value) {
      switch (value) {
        case 0: return UNKNOWN_USER_CARD;
        case 1: return USER_CARD_QUERY_BUILDER;
        case 2: return USER_CARD_QUERY_MANAGEMENT;
        case 3: return USER_CARD_SIGNALS;
        case 4: return USER_CARD_PERFORMANCE_REPORT;
        case 5: return USER_CARD_CUSTOM_RESEARCH_REQUESTS;
        case 6: return USER_CARD_USER_HISTORY;
        case 7: return USER_CARD_INTEGRATION_MANAGEMENT;
        case 8: return USER_CARD_USER_ADMINISTRATION;
        case 9: return USER_CARD_BILLING;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<UserCard>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        UserCard> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<UserCard>() {
            public UserCard findValueByNumber(int number) {
              return UserCard.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return dtos.enums.UserCardEnumProtos.getDescriptor().getEnumTypes().get(0);
    }

    private static final UserCard[] VALUES = values();

    public static UserCard valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private UserCard(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:UserCard)
  }


  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\034protos/enums/user_card.proto*\273\002\n\010UserC" +
      "ard\022\025\n\021UNKNOWN_USER_CARD\020\000\022\033\n\027USER_CARD_" +
      "QUERY_BUILDER\020\001\022\036\n\032USER_CARD_QUERY_MANAG" +
      "EMENT\020\002\022\025\n\021USER_CARD_SIGNALS\020\003\022 \n\034USER_C" +
      "ARD_PERFORMANCE_REPORT\020\004\022&\n\"USER_CARD_CU" +
      "STOM_RESEARCH_REQUESTS\020\005\022\032\n\026USER_CARD_US" +
      "ER_HISTORY\020\006\022$\n USER_CARD_INTEGRATION_MA" +
      "NAGEMENT\020\007\022!\n\035USER_CARD_USER_ADMINISTRAT" +
      "ION\020\010\022\025\n\021USER_CARD_BILLING\020\tB \n\ndtos.enu" +
      "msB\022UserCardEnumProtosb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
