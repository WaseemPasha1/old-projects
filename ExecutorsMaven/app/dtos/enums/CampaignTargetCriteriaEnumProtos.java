// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: protos/enums/campaign_target_criteria.proto

package dtos.enums;

public final class CampaignTargetCriteriaEnumProtos {
  private CampaignTargetCriteriaEnumProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  /**
   * <pre>
   *Every campaign rolled out would be towards a criteria, could be for expired subscription, or new Dataset etc
   * NEXT_TAG 4
   * </pre>
   *
   * Protobuf enum {@code CampaignTargetCriteria}
   */
  public enum CampaignTargetCriteria
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>UNKNOWN_CAMPAIGN_TARGET_CRITERIA = 0;</code>
     */
    UNKNOWN_CAMPAIGN_TARGET_CRITERIA(0),
    /**
     * <code>CAMPAIGN_TARGET_NEW_DATASET = 1;</code>
     */
    CAMPAIGN_TARGET_NEW_DATASET(1),
    /**
     * <code>CAMPAIGN_TARGET_SUBSCRIPTION_EXPIRED = 2;</code>
     */
    CAMPAIGN_TARGET_SUBSCRIPTION_EXPIRED(2),
    /**
     * <pre>
     *To be considered when INTEGRATION features finalized
     *NEW_INTEGRATION = 3;
     * </pre>
     *
     * <code>CAMPAIGN_TARGET_NEW_MATCHING_DATASET = 3;</code>
     */
    CAMPAIGN_TARGET_NEW_MATCHING_DATASET(3),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>UNKNOWN_CAMPAIGN_TARGET_CRITERIA = 0;</code>
     */
    public static final int UNKNOWN_CAMPAIGN_TARGET_CRITERIA_VALUE = 0;
    /**
     * <code>CAMPAIGN_TARGET_NEW_DATASET = 1;</code>
     */
    public static final int CAMPAIGN_TARGET_NEW_DATASET_VALUE = 1;
    /**
     * <code>CAMPAIGN_TARGET_SUBSCRIPTION_EXPIRED = 2;</code>
     */
    public static final int CAMPAIGN_TARGET_SUBSCRIPTION_EXPIRED_VALUE = 2;
    /**
     * <pre>
     *To be considered when INTEGRATION features finalized
     *NEW_INTEGRATION = 3;
     * </pre>
     *
     * <code>CAMPAIGN_TARGET_NEW_MATCHING_DATASET = 3;</code>
     */
    public static final int CAMPAIGN_TARGET_NEW_MATCHING_DATASET_VALUE = 3;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static CampaignTargetCriteria valueOf(int value) {
      return forNumber(value);
    }

    public static CampaignTargetCriteria forNumber(int value) {
      switch (value) {
        case 0: return UNKNOWN_CAMPAIGN_TARGET_CRITERIA;
        case 1: return CAMPAIGN_TARGET_NEW_DATASET;
        case 2: return CAMPAIGN_TARGET_SUBSCRIPTION_EXPIRED;
        case 3: return CAMPAIGN_TARGET_NEW_MATCHING_DATASET;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<CampaignTargetCriteria>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        CampaignTargetCriteria> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<CampaignTargetCriteria>() {
            public CampaignTargetCriteria findValueByNumber(int number) {
              return CampaignTargetCriteria.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return dtos.enums.CampaignTargetCriteriaEnumProtos.getDescriptor().getEnumTypes().get(0);
    }

    private static final CampaignTargetCriteria[] VALUES = values();

    public static CampaignTargetCriteria valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private CampaignTargetCriteria(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:CampaignTargetCriteria)
  }


  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n+protos/enums/campaign_target_criteria." +
      "proto*\263\001\n\026CampaignTargetCriteria\022$\n UNKN" +
      "OWN_CAMPAIGN_TARGET_CRITERIA\020\000\022\037\n\033CAMPAI" +
      "GN_TARGET_NEW_DATASET\020\001\022(\n$CAMPAIGN_TARG" +
      "ET_SUBSCRIPTION_EXPIRED\020\002\022(\n$CAMPAIGN_TA" +
      "RGET_NEW_MATCHING_DATASET\020\003B.\n\ndtos.enum" +
      "sB CampaignTargetCriteriaEnumProtosb\006pro" +
      "to3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
