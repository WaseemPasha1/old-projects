package modules;


import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import play.Application;
import play.Logger;


public class Global {

//    private AnnotationConfigApplicationContext ctx;
//
//    @Inject
//    public Global(DBApi databaseAPI,
//	    DefaultCacheApi cacheApi,
//	    Configuration configuration,
//	    ApplicationLifecycle lifecycle,
//	    Environment environment,
//	    FormFactory formFactory,
//	    play.DefaultApplication playApp) {
//
//	ctx = new AnnotationConfigApplicationContext();
//	ctx.getBeanFactory().registerSingleton("dataSource", databaseAPI.getDatabase("default").getDataSource());
//	ctx.getBeanFactory().registerSingleton("configuration", configuration);
//	ctx.getBeanFactory().registerSingleton("cacheApi", cacheApi);
//	ctx.getBeanFactory().registerSingleton("environment", environment);
//	ctx.getBeanFactory().registerSingleton("formFactory",formFactory);
//	ctx.getBeanFactory().registerSingleton("playApp",playApp);
//	ctx.register(AppConfig.class);
//	ctx.refresh();
//	ctx.registerShutdownHook();
//
//	lifecycle.addStopHook(() -> {
//	    ctx.close();
//	    return CompletableFuture.completedFuture(null);
//	});
//
//    }
//
//    public <A> A getBean(Class<A> clazz) {
//	return ctx.getBean(clazz);
//    }

    @com.google.inject.Inject
    public Global() {
        Logger.info("Application has started");
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                //bind(GreetingService.class).to(RealGreetingService.class);

            }
        });
    }

    private Injector injector;


    public void onStart(Application application) {
        Logger.info("Application has started");
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                //bind(GreetingService.class).to(RealGreetingService.class);

            }
        });
    }


    public <T> T getControllerInstance(Class<T> aClass) throws Exception {
        return injector.getInstance(aClass);
    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }


}