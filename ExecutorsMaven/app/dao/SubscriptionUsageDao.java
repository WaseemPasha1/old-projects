package dao;

public interface SubscriptionUsageDao {

    void saveSubscriptionUsageDatasetTrack() throws Exception;

    void saveSubscriptionUsageSearchTrack() throws Exception;

    void saveSubscriptionUsageEntityTrackCompany() throws Exception;

    void saveSubscriptionUsageEntityTrackContact() throws Exception;



}
