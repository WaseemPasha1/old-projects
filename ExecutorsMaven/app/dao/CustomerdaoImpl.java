package dao;


import entities.Campaign;
import play.api.inject.Module;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Http;
import com.googlecode.protobuf.format.JsonFormat;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.xml.ws.spi.http.HttpContext;
import java.sql.Date;
import java.util.Deque;
import java.util.Set;
import java.util.concurrent.Callable;

public class CustomerdaoImpl implements Customerdao {


    protected JPAApi jpaApi;

//    @Inject
//    protected EntityManager entityManager;
    Notificationdao notificationdao;

    @Inject
    public CustomerdaoImpl(JPAApi jpaApi, Notificationdao notificationdao) {
        this.jpaApi = jpaApi;
        this.notificationdao = notificationdao;

    }


    @Override
    public void saveCampaign() {
        try {
//            Campaign campaign = new Campaign();
//            campaign.setName("Test");
//            campaign.setUsertype("Registered");
//            campaign.setStartdate(java.sql.Date.valueOf("2018-03-31"));
//            campaign.setEndDate(java.sql.Date.valueOf("2018-03-31"));
//            JsonFormat jsonFormat=new JsonFormat();
//            String asJsonString = jsonFormat.printToString(campaign);
//            ObjectMapper objectMapper = new ObjectMapper();
//            JsonNode jsonNode = objectMapper.readTree(asJsonString);
            System.out.println("Starting Notification Table Operations");
//            jpaApi.withTransaction(() -> {
//                EntityManager em = jpaApi.em();
//                em.persist(campaign);
//                em.flush();
//            });
            notificationdao.saveCampaignNotificationByID(1L);
        } catch (Throwable th) {

            throw th;
        }

    }
}
