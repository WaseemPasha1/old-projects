package dao;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.protobuf.format.JsonFormat;
import dtos.CampaignProtos;
import dtos.DateProto;
import dtos.UserSubscriptionUsageProtos;
import entities.SubscriptionUsage;
import entities.campaignproto;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.Calendar;
import java.util.Date;

public class SubscriptionUsageDaoImpl implements SubscriptionUsageDao {

    protected JPAApi jpaApi;

    protected EntityManager getEntityManager() {
        return this.jpaApi.em();
    }


    @Inject
    public SubscriptionUsageDaoImpl(JPAApi jpaApi) { this.jpaApi = jpaApi; }

    @Transactional
    public void saveSubscriptionUsageDatasetTrack() throws Exception {

        try {
            UserSubscriptionUsageProtos.UserSubscriptionUsage.Builder userSubscriptionUsageBuilder = UserSubscriptionUsageProtos.UserSubscriptionUsage.newBuilder();
            UserSubscriptionUsageProtos.ServiceTrackingComponent.Builder serviceTrackingComponentdatasetBuilder =  UserSubscriptionUsageProtos.ServiceTrackingComponent.newBuilder();
            UserSubscriptionUsageProtos.DatasetTrack.Builder datasetTrackBuilder =  UserSubscriptionUsageProtos.DatasetTrack.newBuilder();
            UserSubscriptionUsageProtos.DatasetTrack.OptedDataset.Builder optedDatasetBuilder =  UserSubscriptionUsageProtos.DatasetTrack.OptedDataset.newBuilder();
            UserSubscriptionUsageProtos.SearchTrack.SearchQuery.QueryResult.Builder queryResultBuilder =  UserSubscriptionUsageProtos.SearchTrack.SearchQuery.QueryResult.newBuilder();

            DateProto.Date.Builder dateBuilder = DateProto.Date.newBuilder();
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            dateBuilder.setMonth(cal.get(Calendar.MONTH));
            dateBuilder.setDay(cal.get(Calendar.DATE));
            dateBuilder.setYear(cal.get(Calendar.YEAR));

//DatasetTrack
            optedDatasetBuilder.setName("doctors")
                    .setQueryFilters("CA")
                    .setDate(dateBuilder.build());
            datasetTrackBuilder.setOptedDataset(optedDatasetBuilder).build();
            optedDatasetBuilder.setName("professor")
                    .setQueryFilters("CA")
                    .setDate(dateBuilder.build());
            datasetTrackBuilder.setOptedDataset(optedDatasetBuilder).build();
            serviceTrackingComponentdatasetBuilder.setServiceTypesValue(1)
                    .setDatasetTrack(datasetTrackBuilder).build();
            userSubscriptionUsageBuilder.setUserSubscriptionId(1)
                    .setServiceUsage(serviceTrackingComponentdatasetBuilder);


            SubscriptionUsage usage = new SubscriptionUsage();
            JsonFormat jsonFormat = new JsonFormat();
            String asJsonString = jsonFormat.printToString(userSubscriptionUsageBuilder.build());
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(asJsonString);
            usage.setSubscriptionUsageDetail(userSubscriptionUsageBuilder.build().toByteArray());
            usage.setSubscriptionUsageDetailJson(jsonNode);
            getEntityManager().persist(usage);
            getEntityManager().flush();
            saveSubscriptionUsageSearchTrack();
        } catch (PersistenceException pex) {
            throw pex;
        } catch (Exception ex) {
            throw ex;
        }

    }

    public void saveSubscriptionUsageSearchTrack() throws Exception {

        try {
            UserSubscriptionUsageProtos.UserSubscriptionUsage.Builder userSubscriptionUsageBuilder = UserSubscriptionUsageProtos.UserSubscriptionUsage.newBuilder();
            UserSubscriptionUsageProtos.ServiceTrackingComponent.Builder serviceTrackingComponentdatasetBuilder =  UserSubscriptionUsageProtos.ServiceTrackingComponent.newBuilder();
            UserSubscriptionUsageProtos.SearchTrack.Builder searchTrackBuilder =  UserSubscriptionUsageProtos.SearchTrack.newBuilder();
            UserSubscriptionUsageProtos.DatasetTrack.OptedDataset.Builder optedDatasetBuilder =  UserSubscriptionUsageProtos.DatasetTrack.OptedDataset.newBuilder();
            UserSubscriptionUsageProtos.SearchTrack.SearchQuery.Builder searchQueryBuilder =  UserSubscriptionUsageProtos.SearchTrack.SearchQuery.newBuilder();
            UserSubscriptionUsageProtos.SearchTrack.SearchQuery.QueryResult.Builder queryResultBuilder =  UserSubscriptionUsageProtos.SearchTrack.SearchQuery.QueryResult.newBuilder();


            DateProto.Date.Builder dateBuilder = DateProto.Date.newBuilder();
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            dateBuilder.setMonth(cal.get(Calendar.MONTH));
            dateBuilder.setDay(cal.get(Calendar.DATE));
            dateBuilder.setYear(cal.get(Calendar.YEAR));

//SearchTrack
            optedDatasetBuilder.setName("doc")
                    .setQueryFilters("NY")
                    .setDate(dateBuilder.build());
            queryResultBuilder.setRecordCount(10).build();
            searchQueryBuilder.setDataset(optedDatasetBuilder)
                    .setSearchGuid("2KYF-G")
                    .setSearchQuery("ByDegree")
                    .setDate(dateBuilder.build())
                    .build();
            searchTrackBuilder.setSearchQuery(searchQueryBuilder).build();
            serviceTrackingComponentdatasetBuilder.setServiceTypesValue(2)
                    .setSearchTrack(searchTrackBuilder).build();
            userSubscriptionUsageBuilder.setUserSubscriptionId(1)
                    .setServiceUsage(serviceTrackingComponentdatasetBuilder);


            SubscriptionUsage usage = new SubscriptionUsage();
            JsonFormat jsonFormat = new JsonFormat();
            String asJsonString = jsonFormat.printToString(userSubscriptionUsageBuilder.build());
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(asJsonString);
            usage.setSubscriptionUsageDetail(userSubscriptionUsageBuilder.build().toByteArray());
            usage.setSubscriptionUsageDetailJson(jsonNode);
            getEntityManager().persist(usage);
            getEntityManager().flush();
            saveSubscriptionUsageEntityTrackCompany();
        } catch (PersistenceException pex) {
            throw pex;
        } catch (Exception ex) {
            throw ex;
        }

    }
    @Transactional
    public void saveSubscriptionUsageEntityTrackCompany() throws Exception {

        try {
            UserSubscriptionUsageProtos.UserSubscriptionUsage.Builder userSubscriptionUsageBuilder = UserSubscriptionUsageProtos.UserSubscriptionUsage.newBuilder();

            UserSubscriptionUsageProtos.ServiceTrackingComponent.Builder serviceTrackingComponentdatasetBuilder =  UserSubscriptionUsageProtos.ServiceTrackingComponent.newBuilder();

            UserSubscriptionUsageProtos.EntityTrack.Builder entityTrackCompanyBuilder =  UserSubscriptionUsageProtos.EntityTrack.newBuilder();
            UserSubscriptionUsageProtos.SearchTrack.SearchQuery.QueryResult.Builder queryResultBuilder =  UserSubscriptionUsageProtos.SearchTrack.SearchQuery.QueryResult.newBuilder();
            UserSubscriptionUsageProtos.ViewTrack.Builder viewTrackBuilder = UserSubscriptionUsageProtos.ViewTrack.newBuilder();
            UserSubscriptionUsageProtos.ExportTrack.Builder exportTrackBuilder = UserSubscriptionUsageProtos.ExportTrack.newBuilder();


            DateProto.Date.Builder dateBuilder = DateProto.Date.newBuilder();
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            dateBuilder.setMonth(cal.get(Calendar.MONTH));
            dateBuilder.setDay(cal.get(Calendar.DATE));
            dateBuilder.setYear(cal.get(Calendar.YEAR));


//EntityTrackCompanyVIEW
            viewTrackBuilder.setId(1).build();
            exportTrackBuilder.addIds(1).build();
            exportTrackBuilder.addIds(1).build();
            entityTrackCompanyBuilder
                    .setOnDate(dateBuilder.build())
                    .setSearchGuidRef("companyentity")
                    .setViewed(viewTrackBuilder)
//                    .setExported(exportTrackBuilder)
                    .build();
            serviceTrackingComponentdatasetBuilder.setServiceTypesValue(3)
                    .setEntityTrackCompany(entityTrackCompanyBuilder).build();
            userSubscriptionUsageBuilder.setUserSubscriptionId(1)
                    .setServiceUsage(serviceTrackingComponentdatasetBuilder);

            SubscriptionUsage usage = new SubscriptionUsage();
            JsonFormat jsonFormat = new JsonFormat();
            String asJsonString = jsonFormat.printToString(userSubscriptionUsageBuilder.build());
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(asJsonString);
            usage.setSubscriptionUsageDetail(userSubscriptionUsageBuilder.build().toByteArray());
            usage.setSubscriptionUsageDetailJson(jsonNode);
            getEntityManager().persist(usage);
            getEntityManager().flush();

////EntityTrackCompanyEXPORT
            viewTrackBuilder.setId(1).build();
            exportTrackBuilder.addIds(1).build();
            entityTrackCompanyBuilder
                    .setOnDate(dateBuilder.build())
                    .setSearchGuidRef("companyentity")
//                    .setViewed(viewTrackBuilder)
                    .setExported(exportTrackBuilder)
                    .build();
            serviceTrackingComponentdatasetBuilder.setServiceTypesValue(5)
                    .setEntityTrackCompany(entityTrackCompanyBuilder).build();
            userSubscriptionUsageBuilder.setUserSubscriptionId(1)
                    .setServiceUsage(serviceTrackingComponentdatasetBuilder);

            SubscriptionUsage usage1 = new SubscriptionUsage();
            JsonFormat jsonFormat1 = new JsonFormat();
            String asJsonString1 = jsonFormat1.printToString(userSubscriptionUsageBuilder.build());
            ObjectMapper objectMapper1 = new ObjectMapper();
            JsonNode jsonNode1 = objectMapper1.readTree(asJsonString1);
            usage1.setSubscriptionUsageDetail(userSubscriptionUsageBuilder.build().toByteArray());
            usage1.setSubscriptionUsageDetailJson(jsonNode1);
            getEntityManager().persist(usage1);
            getEntityManager().flush();
            saveSubscriptionUsageEntityTrackContact();
        } catch (PersistenceException pex) {
            throw pex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Transactional
    public void saveSubscriptionUsageEntityTrackContact() throws Exception {

        try {
            UserSubscriptionUsageProtos.UserSubscriptionUsage.Builder userSubscriptionUsageBuilder = UserSubscriptionUsageProtos.UserSubscriptionUsage.newBuilder();
            UserSubscriptionUsageProtos.ServiceTrackingComponent.Builder serviceTrackingComponentdatasetBuilder = UserSubscriptionUsageProtos.ServiceTrackingComponent.newBuilder();
            UserSubscriptionUsageProtos.EntityTrack.Builder entityTrackContactBuilder = UserSubscriptionUsageProtos.EntityTrack.newBuilder();
            UserSubscriptionUsageProtos.ViewTrack.Builder viewTrackBuilder = UserSubscriptionUsageProtos.ViewTrack.newBuilder();
            UserSubscriptionUsageProtos.ExportTrack.Builder exportTrackBuilder = UserSubscriptionUsageProtos.ExportTrack.newBuilder();

            DateProto.Date.Builder dateBuilder = DateProto.Date.newBuilder();
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            dateBuilder.setMonth(cal.get(Calendar.MONTH));
            dateBuilder.setDay(cal.get(Calendar.DATE));
            dateBuilder.setYear(cal.get(Calendar.YEAR));

//EntityTrackContactVIEW
            viewTrackBuilder.setId(1).build();
            exportTrackBuilder.addIds(1).build();
            entityTrackContactBuilder
                    .setOnDate(dateBuilder.build())
                    .setSearchGuidRef("contactentity")
                    .setViewed(viewTrackBuilder)
//                    .setExported(exportTrackBuilder)
                    .build();
            serviceTrackingComponentdatasetBuilder.setServiceTypesValue(4)
                    .setEntityTrackContact(entityTrackContactBuilder)
                    .build();
            userSubscriptionUsageBuilder.setUserSubscriptionId(1)
                    .setServiceUsage(serviceTrackingComponentdatasetBuilder);


            SubscriptionUsage usage = new SubscriptionUsage();
            JsonFormat jsonFormat = new JsonFormat();
            String asJsonString = jsonFormat.printToString(userSubscriptionUsageBuilder.build());
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(asJsonString);
            usage.setSubscriptionUsageDetail(userSubscriptionUsageBuilder.build().toByteArray());
            usage.setSubscriptionUsageDetailJson(jsonNode);
            getEntityManager().persist(usage);
            getEntityManager().flush();

//EntityTrackContactEXPORT
            viewTrackBuilder.setId(1).build();
            exportTrackBuilder.addIds(1).build();
            entityTrackContactBuilder
                    .setOnDate(dateBuilder.build())
                    .setSearchGuidRef("contactentity")
//                    .setViewed(viewTrackBuilder)
                    .setExported(exportTrackBuilder)
                    .build();
            serviceTrackingComponentdatasetBuilder.setServiceTypesValue(6)
                    .setEntityTrackContact(entityTrackContactBuilder)
                    .build();
            userSubscriptionUsageBuilder.setUserSubscriptionId(1)
                    .setServiceUsage(serviceTrackingComponentdatasetBuilder);


            SubscriptionUsage usage2 = new SubscriptionUsage();
            JsonFormat jsonFormat2 = new JsonFormat();
            String asJsonString2 = jsonFormat2.printToString(userSubscriptionUsageBuilder.build());
            ObjectMapper objectMapper2 = new ObjectMapper();
            JsonNode jsonNode2 = objectMapper2.readTree(asJsonString2);
            usage2.setSubscriptionUsageDetail(userSubscriptionUsageBuilder.build().toByteArray());
            usage2.setSubscriptionUsageDetailJson(jsonNode2);
            getEntityManager().persist(usage2);
            getEntityManager().flush();

        } catch (PersistenceException pex) {
            throw pex;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
