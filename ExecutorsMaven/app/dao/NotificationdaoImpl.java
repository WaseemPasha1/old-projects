package dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonNode;
import entities.Campaign;
import entities.Notification;
import net.sf.ehcache.search.aggregator.Sum;
import org.hibernate.query.criteria.internal.expression.function.AggregationFunction;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.Array;
import java.sql.Date;
import java.util.List;

import static org.hibernate.hql.internal.antlr.SqlTokenTypes.SUM;

public class NotificationdaoImpl implements Notificationdao {

    protected JPAApi jpaApi;


    @Inject
    public NotificationdaoImpl(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    @Override
    public Notification saveCampaignNotificationByID(Long Id) {
//         Campaign campaign=null;
//        JsonFormat jsonFormat = new JsonFormat();
        try {
            jpaApi.withTransaction(() -> {

            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
//                CriteriaQuery<Integer> campaignCriteriaQuery = builder.createQuery(Integer.class);
                CriteriaQuery<Campaign> campaignCriteriaQuery = builder.createQuery(Campaign.class);
            Root<Campaign> campaignRoot = campaignCriteriaQuery.from(Campaign.class);
            campaignCriteriaQuery.select(campaignRoot);
//            campaignCriteriaQuery.select(builder.sum(campaignRoot.get("v_id")));

//                    .where(campaignRoot.get("id").in(Id));
             List campaign = getEntityManager().createQuery(campaignCriteriaQuery).getResultList();
              Object[] campaignArray = campaign.toArray();
                System.out.println(campaignArray[0]);
//              String name = campaign[0].getName();
//                String v_name  = campaign[0].getV_name();
//                System.out.println("Virtual column name:" +v_name);
//            String usertype = campaign.getUsertype();
//            Date sdate = campaign.getStartDate();
//            Date edate = campaign.getEndDate();
//            Notification notification = new Notification();
//            notification.setName(campaign.getName());
//            notification.setUsertype(usertype);
//            notification.setStartdate(sdate);
//            notification.setEndDate(edate);

//                getEntityManager().persist(notification);
                getEntityManager().flush();
            });


//            Notification notification = new Notification();
//            Campaign campaign = new Campaign();
//            notification.getName();
//            campaign.setUsertype("Registered");
//            campaign.setStartdate(java.sql.Date.valueOf("2018-03-31"));
//            campaign.setEndDate(java.sql.Date.valueOf("2018-04-01"));

//
        } catch (Throwable th) {

            throw th;
        }
        return null ;
    }

    public EntityManager getEntityManager() {
        return this.jpaApi.em();
    }
}
