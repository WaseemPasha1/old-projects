package dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import dtos.*;
import dtos.NotificationProtos;
import dtos.enums.CampaignComponentsEnumProtos;
import dtos.enums.CampaignDeliveryAlertEnumProtos;
import entities.*;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class campaignDaoImpl implements campaigndao {


    protected JPAApi jpaApi;


    protected EntityManager getEntityManager() {
        return this.jpaApi.em();
    }

    @Inject
    public campaignDaoImpl(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }


    /**
     * Synchronize the persistence context of Campaign Entity to the underlying database.
     * A Campaign can only be created. An existing campaign cannot be updated
     *
     * @throws PersistenceException if the flush fails
     * @throws Exception            if the campaignProto is not well defined
     */

    @Transactional
    public void saveNewCampaign() throws Exception {

        try {
            CampaignProtos.Campaign campaign = CampaignProtos.Campaign.newBuilder()
                    .setName("kim")
//                    .setStartDate()
                    .build();
            campaignproto camp = new campaignproto();
            JsonFormat jsonFormat = new JsonFormat();
            String asJsonString = jsonFormat.printToString(campaign);
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(asJsonString);
camp.setCampaignDetail(campaign.toByteArray());
camp.setCampaignDetailJson(jsonNode);
            getEntityManager().persist(camp);
            getEntityManager().flush();
        } catch (PersistenceException pex) {
            throw pex;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
