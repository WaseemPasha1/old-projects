package entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class),
        @TypeDef(name = "int-array", typeClass = IntArrayType.class),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class),
        @TypeDef(name = "json-node", typeClass = JsonNodeStringType.class),
})


@Entity
@Table(name = "SubscriptionUsage")
public class SubscriptionUsage {
    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "SubscriptionUsage_Id", unique = true, nullable = false)
    private int subscriptionUsageId;

    @Column(name = "SubscriptionUsage_Id_Detail")
    private byte[] subscriptionUsageDetail;


    @Type(type = "json")
    @Column(columnDefinition = "json", name = "SubscriptionUsage_Id_Detail_Json")
    private JsonNode subscriptionUsageDetailJson;


    public void setSubscriptionUsageId(int subscriptionUsageId) { this.subscriptionUsageId = subscriptionUsageId; }

    public int getSubscriptionUsageId() { return subscriptionUsageId; }

    public void setSubscriptionUsageDetail(byte[] subscriptionUsageDetail) { this.subscriptionUsageDetail = subscriptionUsageDetail; }

    public byte[] getSubscriptionUsageDetail() { return subscriptionUsageDetail; }

    public void setSubscriptionUsageDetailJson(JsonNode subscriptionUsageDetailJson) { this.subscriptionUsageDetailJson = subscriptionUsageDetailJson; }

    public JsonNode getSubscriptionUsageDetailJson() { return subscriptionUsageDetailJson; }
}
