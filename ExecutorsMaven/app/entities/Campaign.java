package entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.*;
import play.libs.Json;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class),
        @TypeDef(name = "int-array", typeClass = IntArrayType.class),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class),
        @TypeDef(name = "json-node", typeClass = JsonNodeStringType.class),
})

@Entity
@Table(name="campaign")
public class Campaign {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

//    @Column(name = "firstname")
//    private String firstname;

    @Column(name = "usertype")
    private String usertype;

    @Column(name = "startdate")
    private Date startdate;

    @Column(name = "enddate")
    private Date enddate;

    @Type(type = "json")
    @Column(columnDefinition = "json", name="campaignObject")
    private JsonNode campaign;

//    @Generated(GenerationTime.ALWAYS)
//    @Formula("`campaignObject` -> '$.name'")
//    @Type(type = "varchar(10)")
    @Column(columnDefinition = "varchar(20) GENERATED ALWAYS AS(`campaignObject` -> '$.name')", insertable=false, updatable=false)
    private String v_name;

    @Column(columnDefinition = "int(20) GENERATED ALWAYS AS(`campaignObject` -> '$.id')", insertable=false, updatable=false)
    private String v_id;

    @Column(columnDefinition = "varchar(20) GENERATED ALWAYS AS(`campaignObject` -> '$.startDate')", insertable=false, updatable=false)
    private String v_startDate;

    @Column(columnDefinition = "varchar(20) GENERATED ALWAYS AS(`campaignObject` -> '$.endDate')", insertable=false, updatable=false)
    private String v_endDate;

//    @OneToMany
//    private List<Article> articles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    public String getfirstname() { return firstname;}
    //    public void setfirstname(String firstname) { this.firstname = firstname;}

    public String getUsertype() { return usertype;}

    public void setUsertype(String usertype) { this.usertype = usertype;}

    public Date getStartDate() { return startdate; }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEndDate() { return enddate; }

    public void setEndDate(Date startdate) {
        this.enddate = enddate;
    }

    //    public Json getCampaign() { return campaign; }
    //    public void setCampaign(Json campaign) { this.campaign = campaign; }

    public JsonNode getCampaign() { return campaign; }

    public void setCampaign(JsonNode campaign) { this.campaign = campaign; }

    public String getV_name() { return v_name; }

    public String getV_id() { return v_id; }


    //    public List<Article> getArticles() {
    //        return articles;
    //    }

    //    public void setArticles(List<Article> articles) {
    //        this.articles = articles;
    //    }

}
