package entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class),
        @TypeDef(name = "int-array", typeClass = IntArrayType.class),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class),
        @TypeDef(name = "json-node", typeClass = JsonNodeStringType.class),
})


@Entity
@Table(name = "campaignproto")
public class campaignproto {
    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "Campaign_Id", unique = true, nullable = false)
    private int campaignId;

    @Column(name = "Campaign_Detail")
    private byte[] campaignDetail;


    @Type(type = "json")
    @Column(columnDefinition = "json", name = "Campaign_Detail_Json")
    private JsonNode campaignDetailJson;

    public void setCampaignId(int campaignId) { this.campaignId = campaignId; }

    public int getCampaignId() { return campaignId; }

    public void setCampaignDetail(byte[] campaignDetail) { this.campaignDetail = campaignDetail; }

    public byte[] getCampaignDetail() { return campaignDetail; }

    public void setCampaignDetailJson(JsonNode campaignDetailJson) { this.campaignDetailJson = campaignDetailJson; }

    public JsonNode getCampaignDetailJson() { return campaignDetailJson; }
}
