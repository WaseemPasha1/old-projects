package entities;

import javax.persistence.*;
import java.sql.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="notification")
public class Notification {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "usertype")
    private String usertype;

    @Column(name = "startdate")
    private Date startdate;

    @Column(name = "enddate")
    private Date enddate;

//    @OneToMany
//    private List<Article> articles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public Date getStartDate() { return startdate; }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEndDate() { return enddate; }

    public void setEndDate(Date startdate) {
        this.enddate = enddate;
    }

//    public List<Article> getArticles() {
//        return articles;
//    }

//    public void setArticles(List<Article> articles) {
//        this.articles = articles;
//    }

}

