package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;
import dao.Customerdao;
import dao.CustomerdaoImpl;
import dao.SubscriptionUsageDao;
import dao.campaigndao;
import entities.Campaign;
import org.jdbcdslog.CallableStatementLoggingHandler;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.*;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.sql.CallableStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
@Singleton
public class HomeController extends Controller {

    Customerdao customerdao;
    campaigndao campaigndao;
    SubscriptionUsageDao subscriptionUsageDao;
    private ScheduledExecutorService scheduledExecutorService;

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }


    @Inject
    public HomeController(Customerdao customerdao, campaigndao campaigndao, SubscriptionUsageDao subscriptionUsageDao) {
        this.customerdao = customerdao;
        this.campaigndao = campaigndao;
        this.subscriptionUsageDao = subscriptionUsageDao;
    }

    @Transactional
    public Result getListA() {

//            ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);
//                        customerdao.saveCategory();
        this.scheduledExecutorService = Executors.newScheduledThreadPool(2);

        List<String> dateList = Arrays.asList("03-22-2018 40800", "03-10-2018 50800", "03-02-2018 20800", "03-24-2018 10800", "03-28-2018 70800", "03-25-2018 80800", "03-01-2018 40900", "03-29-2018 70200", "03-09-2018 40200");
        Queue<Date> unsortedDateQueue = new PriorityQueue<Date>();
        Queue<Date> sortedDateQueue = new PriorityQueue<Date>();
        try {
            if (dateList != null) {
                dateList.forEach(value -> {
                    int timevalue = Integer.parseInt(value.substring(11));
                    int hours = timevalue / 3600;
                    int remainder = timevalue - hours * 3600;
                    long minutes = remainder / 60;
                    String time = String.valueOf(hours + ":" + minutes);
                    DateFormat format = new SimpleDateFormat("MM-dd-yyyy HH:mm");
                    try {
                        Date date = format.parse(value.substring(0, 10) + " " + time);
                        unsortedDateQueue.add(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
//                    System.out.println(dateQueue);
                });
                List<Date> result = unsortedDateQueue.stream().sorted().collect(Collectors.toList());
                if (result != null) {
                    sortedDateQueue.addAll(result);
                    System.out.println(sortedDateQueue);
                    for (int i = 0; i <= 3; i++) {
//                                            customerdao.saveCampaign();
                        subscriptionUsageDao.saveSubscriptionUsageDatasetTrack();
                    }
                    campaigndao.saveNewCampaign();
                }

            }
        } catch (Exception e) {
            System.err.println("Caught IOException: " + e.getMessage());
        }

        Callable startService = () -> {
            System.out.println("Executing startService Logs At " + System.nanoTime());

            try {
                if (sortedDateQueue.element().before(new Date())) {
                    System.out.println("startService" + sortedDateQueue.element());
//                    customerdao.saveCampaign();
//                    subscriptionUsageDao.saveSubscriptionUsageDatasetTrack();

                    System.out.println("Record Successfully Inserted In The Database");
                }
            } finally {

                return null;

            }
        };


        System.out.println("Submitting task at " + System.nanoTime() + " to be executed after 2 second.");


        Runnable runnable = () -> {
            Future future1 = scheduledExecutorService.submit(startService);
        };

        //The scheduler to run every second.The scheduler runs the method runnable.

        scheduledExecutorService.scheduleAtFixedRate(runnable, 1, 2, TimeUnit.SECONDS);
        return ok("done");
    }

}

