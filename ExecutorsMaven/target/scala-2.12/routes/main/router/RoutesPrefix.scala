// @GENERATOR:play-routes-compiler
// @SOURCE:/home/saqib/projects/trials/play-java-seed/conf/routes
// @DATE:Tue May 08 12:06:34 IST 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
