--
-- Sample dataset containing Dummp data :)
--

-- =================================================================================================

-- createSch Schema
-- campaign Table
--insert into campaign(enddate, name, startdate, usertype, campaignObject) values ('2018-02-04', 'Tom B. Erichsen', '2018-02-04', 'Stavanger','{"idh": "1", "name": "Fred"}')
insert into campaign(enddate, name, startdate, usertype,campaignObject) values ('2018-02-28', 'Tom', '2018-02-01', 'Registered','{"name": "Silver Package", "money": {"unit": 199}, "description": "Silver Package", "billing_cycle": {"interval": "BILLING_INTERVAL_MONTH"}, "subscription_status": "SUBSCRIPTION_STATUS_ACTIVE", "subscription_component": [{"dataset_component": {"number_of_datasets": 2}, "subscription_components": "SUBSCRIPTION_COMPONENT_DATASET"}, {"search_component": {"number_of_searches_per_day": 300, "number_of_searches_per_month": 20}, "subscription_components": "SUBSCRIPTION_COMPONENT_SEARCHES"}, {"contact_component": {"number_of_contacts_view_per_month": 100, "number_of_contacts_download_per_month": 5}, "subscription_components": "SUBSCRIPTION_COMPONENT_CONTACTS"}, {"integration_component": {}, "subscription_components": "SUBSCRIPTION_COMPONENT_INTEGRATIONS"}]}')
insert into campaign(enddate, name, startdate, usertype,campaignObject) values ('2018-03-31', 'Erichsen', '2018-03-01', 'UnRegistered','{"id": "2", "name": "John", "startDate":"2018-03-01", "endDate":"2018-03-31"}')
insert into campaign(enddate, name, startdate, usertype,campaignObject) values ('2018-04-30', 'Thomas', '2018-04-01', 'Registered','{"id": "3", "name": "Jack", "startDate":"2018-04-01", "endDate":"2018-04-30"}')
insert into campaign(enddate, name, startdate, usertype,campaignObject) values ('2018-05-31', 'Anthony', '2018-05-01', 'UnRegistered','{"id": "4", "name": "Sean", "startDate":"2018-05-01", "endDate":"2018-05-31"}')
insert into campaign(enddate, name, startdate, usertype,campaignObject) values ('2018-06-30', 'Tony', '2018-06-01', 'Registered','{"id": "5", "name": "Loyd", "startDate":"2018-06-01", "endDate":"2018-06-30"}')
insert into campaign(enddate, name, startdate, usertype,campaignObject) values ('2018-07-31', 'Anna', '2018-07-01', 'UnRegistered','{"id": "6", "name": "Rose", "startDate":"2018-07-01", "endDate":"2018-07-31"}')



--INSERT INTO campaign (enddate, name, startdate, usertype)
--VALUES ('2018-02-04','Tom B. Erichsen','2018-02-04','Stavanger');