name := """play-java-seed"""
organization := "com.example"

version := "1.0-SNAPSHOT"

routesGenerator := StaticRoutesGenerator

lazy val root = (project in file(".")).enablePlugins(PlayJava)

libraryDependencies ++= Seq(
  javaJpa,
  cache,
  javaWs,
  "org.hibernate" % "hibernate-entitymanager" % "5.2.5.Final" exclude("dom4j", "dom4j"),
  "mysql" % "mysql-connector-java" % "6.0.5",
  "dom4j" % "dom4j" % "1.6.1" intransitive(),
  "com.googlecode.protobuf-java-format" % "protobuf-java-format" % "1.4",
  // https://mvnrepository.com/artifact/com.google.protobuf/protobuf-java
  "com.google.protobuf" % "protobuf-java" % "3.5.0",
  // https://mvnrepository.com/artifact/com.googlecode.protobuf-java-format/protobuf-java-format
  "com.googlecode.protobuf-java-format" % "protobuf-java-format" % "1.4"
)
libraryDependencies += evolutions
libraryDependencies += guice
unmanagedJars in Compile += file("conf/test-1.0-SNAPSHOT.jar")
